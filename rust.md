# My crates list

## Macros ##
* derive_more -- дополнительные derive стандартных треитов

## Email ##
* lettre -- отправка email
* lettre_email 

## Regex ##
* regex

## Parsing ##
* clap -- Парсер аргуентов командной строки в стиле unix
* pest -- Парсинг чего угодно
* addr -- Парсинг доменного имени и email

## Render ##

* wgpu -- WebGPU implementation
* vulkano -- safe Vulkan binds
* rendy -- hight level 'gfx-hal' wrapper
* ash -- lightweight Vulkan wrapper
* euc -- CPU render library
* rayn -- This is a CPU-based path tracing renderer focused on rendering SDFs, specifically fractals.
* luminance -- 
* glium -- (depr) OpenGL and GlES safe library

## Window and input ##
* winit -- 
* gilrs -- gamepad library

## Audio ##
* rodio -- playback
* cpal -- low-level audi IO lib 
* minimp3 -- MP3 decoding
* hound -- WAV decoding .
* lewton -- Vorbis decoding .
* claxon -- Flac decoding .

## Math ##

* nalgebra -- linear algebra
* mint -- 
* cgmath -- 

## Physic ##

* ncollide -- physics collide library
* nphysics2d -- 2d physic library
* nphysics3d -- 3d physics library

## Std Hashing ##
* fxhash -- fxhash impl

## Collections and Data Structures ##
* ndarray -- N dimentions arrays
* petgraph -- Graph data structure library. Provides graph types and graph algorithms.
* smallvec -- optimization of vec for store small elements on stack
* slab -- Pre-allocated storage for a uniform data type.
* hibitset -- Provides hierarchical bit sets, which allow very fast iteration on sparse data structures.

## Async and Parallelism ##
* mio -- async IO
* async-std -- provides an async version of std. It provides all the interfaces you are used to, but in an async version and ready for Rust's async/await syntax.
* tokio -- A runtime for writing reliable, asynchronous, and slim applications with the Rust programming language. It is:
* futures -- Set of based Futures
* rayon -- Data-parallelism library that makes it easy to convert sequential computations into parallel

## Serialize and Data Storage Formats ##
* serde -- core serialization library
* rkyv -- binary serialize
* toml -- toml library
* yaml-rust -- yaml lib
* ron -- Rusty Objext Notation. Формат хорош для сохранения структур данных раста в удобочитаемом и РЕДАКТИРУЕМОМ стиле.
* bincode -- binary serialization format

## Data Compression ##
* flate2 -- deflate impl


## Web ##
* actix-web -- web server

## Entity-Component-System (ECS) ##
* specs -- Specs Parallel ECS
* legion -- Legion aims to be a feature rich high performance Entity component system (ECS) library for Rust game projects with minimal boilerplate.

## Game Engines ##
* amethyst (Разработка прекращена)
* bevy
* macroquad
* scion
* rg3d
* tetra

## Other ##

* rand -- random library
* bitflags -- bit flags
* either -- The enum Either with variants Left and Right is a general purpose sum type with two cases.
* rusttype -- fonts
* obj -- .obj loader
* pareen -- animation (game animation)
* dirs -- The library provides the location of these directories by leveraging the mechanisms defined by
* downcast-rs -- Some applications may want to cast these trait objects back to the original concrete types to access additional functionality and performant inlined implementations.
downcast-rs adds this downcasting support to trait objects using only safe Rust. It supports type parameters, associated types, and constraints.
* thread_profiler
* simba --  SIMD algebra for the Rust programming language. 
* semver -- Semantic Version parser
* shred -- This library allows to dispatch systems, which can have interdependencies, shared and exclusive resource access, in parallel.
* shrev -- A pull based event channel, with events stored in a ring buffer, meant to be used as a resource in specs.
* rlua -- high level interface between Rust and Lua. Its goal is to be an easy to use, practical, flexible, and safe API between Rust and Lua.

* **hyper**: A fast and correct HTTP/1.1 and HTTP/2 implementation for Rust.

* **tonic**: A gRPC over HTTP/2 implementation focused on high performance, interoperability, and flexibility.

* **warp**: A super-easy, composable, web server framework for warp speeds.

* **tower**: A library of modular and reusable components for building robust networking clients and servers.

* **tracing (formerly tokio-trace)**: A framework for application-level tracing and async-aware diagnostics.

* **rdbc**: A Rust database connectivity library for MySQL, Postgres and SQLite.

* **mio**: A low-level, cross-platform abstraction over OS I/O APIs that powers tokio.

* **bytes**: Utilities for working with bytes, including efficient byte buffers.

* **loom**: A testing tool for concurrent Rust code

